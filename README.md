Teste para vaga de Desenvolvedor Back-end
Criar uma API REST para aplicativo de doações


--Cadastro de usuarios
nome
email
senha
tipo de ususario: (admin, entidade)

--Cadastro de entidades

Nome;
CNPJ;
Endereço;
Telefone;
Descrição:
Imagem de capa:
Imagem de perfil:
Video institucional (link youtube):
** usuario ( uma empresa pode ter mais de um usuario vinculado )


--Evento
Nome do evento
Descricao
video institucional
Foto de capa
entidade
data do evento
duraçao 

--Doador (pessoa fisica)
Nome
CPF
email
celular
endereço
foto de perfil


--Doacoes
doador
entidade
evento
valor
data


Todos os campos são de preenchimento obrigatório.

Funcionalidades
Painel admin com ACL (adminstrador, e entidade)
- usuario admin pode ter acesso a tudo, usuario entidade sómente aos dados relacionados a sua entidade

Admin:
	- Cadastro de entidades
	- Cadastro de usuarios
Entidade
	- CRUD de eventos
	- Relatorio doacoes
	- Relatorio doadores
	
API:
 autenticação JWT
 endepoint para listagem de entidades
 endpoint para listagem de eventos
 endpoint para criar uma dooação
 endpoint para visualizar uma entidade
 endpoint para visualizar um evento
 